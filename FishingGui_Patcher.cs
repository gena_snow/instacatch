﻿using Harmony;
using System;
using System.Reflection;

namespace InstaCatch
{

    [HarmonyPatch(typeof(FishingGUI))]
    [HarmonyPatch("UpdatePulling")]
    internal class FishingGUI_UpdatePulling_Patch
    {
        [HarmonyPostfix]
        // This method should instantly make the pull successful, landing the fish.
        public static void Postfix(FishingGUI __instance)
        {

            Type fgType = typeof(FishingGUI);

            // Set "is_success_fishing" to true, indicating that we've finished the
            // minigame and caught the fish
            FieldInfo success_Field = fgType.GetField("is_success_fishing", BindingFlags.NonPublic | BindingFlags.Instance);
            success_Field.SetValue(__instance, true);

            // Changing the fishing state from "Pulling on the fish" to "Taking the fish
            // out of the water"
            FieldInfo _stateField = fgType.GetField("_state", BindingFlags.NonPublic | BindingFlags.Instance);
            _stateField.SetValue(__instance, FishingGUI.FishingState.TakingOut);

            return;

        }

    }

}