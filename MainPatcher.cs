﻿using System.Reflection;
using Harmony;

namespace InstaCatch
{
    public class MainPatcher
    {
        public static void Patch()
        {
            var harmony = HarmonyInstance.Create("com.gena.graveyardkeeper.instacatch.mod");   // Change this line to match your mod. 
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }

    }
}
